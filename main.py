# Skeleton Program for the AQA AS Summer 2018 examination
# this code should be used in conjunction with the Preliminary Material
# written by the AQA AS Programmer Team
# developed in a Python 3 environment


# Version Number : 1.6

import pickle, re

SPACE = ' '
EOL = '#'
EMPTYSTRING = ''

def ReportError(s):
	print('{0:<5}'.format('*'),s,'{0:>5}'.format('*')) 

def get_transmission():
	FileName = input("Enter file name: ")
	try:
		FileHandle = open(FileName, 'r')
		Transmission = FileHandle.readline()
		FileHandle.close()
		Transmission = Transmission.rstrip()
		if len(Transmission) > 0:
			Transmission = Transmission.lstrip()
			Transmission = Transmission + EOL
	except:
		ReportError("No transmission found")
		Transmission = EMPTYSTRING
	return Transmission

def GetNextSymbol(i, Transmission):
	if Transmission[i] == EOL:
		print()
		print("End of transmission")
		Symbol = EMPTYSTRING
	else:
		SymbolLength = 0
		Signal = Transmission[i]

		# Finds out how long a string is until an space or end of line
		while Signal != SPACE and Signal != EOL:
			i += 1
			Signal = Transmission[i]
			SymbolLength += 1

		if SymbolLength == 1:
			Symbol = '.'
		elif SymbolLength == 3:
			Symbol = '-'
		elif SymbolLength == 0: 
			Symbol = SPACE
		else:
			ReportError("Non-standard symbol received") 
			Symbol = EMPTYSTRING
	return i, Symbol 

def GetNextLetter(i, Transmission):
	SymbolString = EMPTYSTRING
	LetterEnd = False
	while not LetterEnd:
		i, Symbol = GetNextSymbol(i, Transmission)
		if Symbol == SPACE:
			LetterEnd = True
			i += 4
		elif Transmission[i] == EOL:
			LetterEnd = True
		elif Transmission[i + 1] == SPACE and Transmission[i + 2] == SPACE:
			LetterEnd = True
			i += 3
		else:
			i += 1
		SymbolString = SymbolString + Symbol
	return i, SymbolString

def Decode(CodedLetter, Dash, Letter, Dot):
	pointer = 0
	for morse_code_char in CodedLetter:
		if morse_code_char == SPACE:
			return SPACE
		elif morse_code_char == '-':
			pointer = Dash[pointer]
		else:
			pointer = Dot[pointer]
	return Letter[pointer]

def ReceiveMorseCode(Dash, Letter, Dot) -> tuple[str, str]: 
	decoded_text = EMPTYSTRING
	morse_code = EMPTYSTRING
	transmission = get_transmission()
	last_char = len(transmission) - 1

	i = 0
	while i < last_char:
		i, morse_code_char = GetNextLetter(i, transmission)
		decoded_char = Decode(morse_code_char, Dash, Letter, Dot)

		morse_code = morse_code + SPACE + morse_code_char
		decoded_text = decoded_text + decoded_char
	
	print(morse_code)
	print(decoded_text)

	return (morse_code, decoded_text)

def SendMorseCode(MorseCode):
	PlainText = input("Enter your message (letters and spaces only): ").upper()
	PlainTextLength = len(PlainText)
	MorseCodeString = EMPTYSTRING
	for i in range(PlainTextLength):
		PlainTextLetter = PlainText[i]
		if PlainTextLetter == SPACE:
			Index = 0
		else: 
			Index = ord(PlainTextLetter) - ord('A') + 1
		CodedLetter = MorseCode[Index]
		MorseCodeString = MorseCodeString + CodedLetter + SPACE
	print(MorseCodeString)

	return (MorseCodeString, PlainText)


def convert_char_to_transmission(char: str) -> str:
	match char:
		case " ":
			return "  "
		case "-":
			return "=== "
		case ".":
			return "= "
		case "  ":
			return "    "

def convert_to_transmission(morse_code: str) -> str:
	morse_code_words = re.split("  .", morse_code)
	transmission = EMPTYSTRING
	for word in morse_code_words:
		for char in word:
			transmission += convert_char_to_transmission(char)
		transmission += "      "
	return transmission

def dump_data(file: str, data):
	with open(file, "w+b") as file:
		pickle.dump(data, file)

def save_menu():
	print("Where do you want to save your data?")
	return input(": ")

def DisplayMenu():
	print()
	print("Main Menu")
	print("======================")
	print("R - Receive Morse code")
	print("S - Send Morse code")
	print("O - Output Text Buffer")
	print("D - Dump Text Buffer")
	print("X - Exit program")
	print()

def GetMenuOption():
	return input("Enter your choice: ").upper()
		
def SendReceiveMessages():
	Dash = [20,23,0,0,24,1,0,17,0,21,0,25,0,15,11,0,0,0,0,22,13,0,0,10,0,0,0]
	Dot = [5,18,0,0,2,9,0,26,0,19,0,3,0,7,4,0,0,0,12,8,14,6,0,16,0,0,0]
	Letter = [' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

	MorseCode = [' ','.-','-...','-.-.','-..','.','..-.','--.','....','..','.---','-.-','.-..','--','-.','---','.--.','--.-','.-.','...','-','..-','...-','.--','-..-','-.--','--..']
	morse_code, decoded_text, transmission = EMPTYSTRING, EMPTYSTRING, EMPTYSTRING

	while True:
		DisplayMenu()
		match GetMenuOption():
			case "R":
				morse_code, decoded_text = ReceiveMorseCode(Dash, Letter, Dot)
				transmission = convert_to_transmission(morse_code)
			case "S":
				morse_code, decoded_text = SendMorseCode(MorseCode) 
				transmission = convert_to_transmission(morse_code)
			case "O":
				print(f"Plain Text:   {decoded_text}")
				print(f"Morse Code:   {morse_code}")
				print(f"Transmission: {transmission}")
			case "D":
				file_location = save_menu()
				dump_data(file_location, [morse_code, decoded_text, transmission])
			case "L":
				pass
			case "X":
				break
			case _:
				print("Non-valid input")

if __name__ == "__main__":
	SendReceiveMessages()
